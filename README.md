# Multi Brute Force

cookies method with python2 version
![MBF]()

## Installation
```
pkg update && pkg upgrade
pkg install git 
git clonehttps://gitlab.com/hotaru.yu666/mbf
pkg install python2
pip2 install requests bs4
pip2 install mechanize
```

## Run script
```
cd mbf
python2 run.py
```

## Contact
[Facebook](https://www.facebook.com/hhva)
[Telegram](https://t.me/)
